﻿using System;

public class RecordInConsole
{   
    public static int Main(string[] args)
    {
        string openMessage = "Type to continue, enter CANCEL to cancel, enter STOP to stop recording and push data to file";

        List<string> inputs = new List<string>();
        Console.WriteLine(openMessage);
        string line;
        while ((line = Console.ReadLine()) != null)
        {
            if (line.Equals("stop", StringComparison.OrdinalIgnoreCase))
                break;
            if (line.Equals("cancel", StringComparison.OrdinalIgnoreCase))
                return 0;
            if (line.Equals(""))
                continue;
            inputs.Add(line);
        }

        Console.WriteLine("Writing data to file. . .");

        //get the patch to MyDocuments
        string docPath =
          Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

        // Write the string array to a new file
        using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, "RecordedData-" + DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".txt")))
        {
            foreach (string data in inputs)
            {
                outputFile.WriteLine(data);
            }
        }
        return 0;
    }
}
